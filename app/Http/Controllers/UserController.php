<?php 

namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Services\UserService;
use App\Services\UtilityService;
use App\Services\OtpProcessService;
use App\Services\UserVerifyService;
use JWTAuth;

class UserController extends Controller {

    public function login(Request $request) {
        $credentials = User::buildCredential($request);
        $validator = Validator::make($credentials, User::loginRules());
        $user = User::getByIdentity($request->identity);
        if($user == null) $validator->errors()->add('login', 'Invalid Credentials');

        if($user) {
            $validator->after(function ($validator) use ($request, $user) {
                if(!$user->verifyPassword($request->password)) {
                    $validator->errors()->add('login', 'Invalid Credentials');
                }
            });
        }
        
        if($validator->fails()) return $this->errorResponse('Unauthorized', $validator->errors(), self::$HTTP_UNAUTHORIZED);
        if(!$token = auth()->attempt($credentials)) return $this->errorResponse('Unauthorized', ['Token' => 'Token invalid'], self::$HTTP_UNAUTHORIZED);

        $status = UtilityService::sendLoginEmail($request->identity);
        return $this->response('success', [
            'access_token' => $token, 
            'token_type' => 'bearer', 
            'user' => auth()->user()
        ]);
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), User::validateRules());
        if($validator->fails()) return $this->errorResponse('Validation Error', $validator->errors());

        $user = UserService::createUser($request->all());
        if(!$user) return $this->response('Failed', []);

        $status = UtilityService::sendRegistrationEmail($request->email);
        if(!$status) {
            return $this->errorResponse('Server Error',[],self::$HTTP_INTERNL_SERVER_ERROR);
        }
        return $this->response('success', $user);
    }

    public function logout(Request $request) {
        auth()->logout();
        return $this->response('User logged out successfully');
    }

    public function profile() {
        return $this->response('Success',auth()->user());
    }

    public function sendOtp(User $user) {
        $otp_status = OtpProcessService::processOtp($user);

        if($otp_status == 0) {
            return $this->response('Success: sent');
        }
        if($otp_status == 1) {
            return $this->response('Please request administrator to enable the service');
        }
        return $this->errorResponse('Server Error',[],self::$HTTP_INTERNL_SERVER_ERROR);
    }

    public function changePassword(Request $request) {
        $authUser = auth()->user();
        $validator = Validator::make($request->all(), [
            'old_password' => ['required','string'],
            'new_password' => ['required','string'],
            'email_otp' => ['numeric']
        ]);

        $user = UserService::getUserById($authUser->id);
        $validator->after(function ($validator) use ($request, $user) {
            if(!$user->verifyPassword($request->old_password)) {
                $validator->errors()->add('login', 'Invalid credentials');
            }elseif(!UserService::checkUserOtp($user, $request->all())) {
                $validator->errors()->add('login', 'Invalid credentials');
            }
        });

        if ($validator->fails()) {
            return $this->errorResponse('Validation Error',$validator->errors());
        }

        $status = UserService::updateUserCred($user, $request->all());
        if($status){
            return $this->response('Success');
        }
        return $this->response('Failed to update',[], self::$HTTP_INTERNL_SERVER_ERROR);
    }

    public function forgetPassword() {
        $validator = Validator::make($request->all(), [
            'email' => ['required','email','string']
        ]);

        if ($validator->fails()) {
            return $this->errorResponse('Validation Error',$validator->errors());
        }

        $status = OtpProcessService::sendUserOtp($request->email);
        if($status == 1) {
            return $this->response('Success: sent');
        }
        if($status == 0){
            return $this->response('Invalid email');
        }
        return $this->response('Failed to send',[], self::$HTTP_INTERNL_SERVER_ERROR);
    }

    public function updatePassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'email_otp' => ['required','numeric'],
            'new_password' => ['required','string']
        ]);

        $validator->after(function ($validator) use ($request) {
            if(!UserService::checkUserOtp(null, $request->all())) {
                $validator->errors()->add('login', 'Invalid otp');
            }
        });

        if ($validator->fails()) {
            return $this->errorResponse('Validation Error',$validator->errors());
        }

        $status = UserService::updateUserPassword($request);
        if($status) {
            return $this->response('Success');
        }
        return $this->errorResponse('Server Error',[],self::$HTTP_INTERNL_SERVER_ERROR);
    }
}