<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerificationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data = [];
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($otpData)
    {
        $this->data = $otpData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build($data)
    {
        return $this->view('emails.VerificationMail');
    }
}
