<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

if (! function_exists('httpGet')) {
    function httpGet($url, array $headers = []) {
        try {
            $options = [
                "headers" => $headers,
            ];
            $client = new Client();
            $response = $client->get($url, $options);
            $returnResponse->status = $response->getStatusCode();
            $returnResponse->body = json_decode(Psr7\stream_for($response->getBody()));
            return $returnResponse;
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $returnResponse->status = $response->getStatusCode();
            $returnResponse->body = json_decode(Psr7\stream_for($response->getBody()));
            return $returnResponse;
        }
        $returnResponse->body = 'Oops server error';
        return $returnResponse;
        
    }
}

if (! function_exists('httpPost')) {
    function httpPost($url, array $body = [], array $headers = []) {
        $returnResponse = new \stdClass();
        $returnResponse->status = 500;
        try {
            $options = [
                "headers" => $headers,
                "body" => json_encode($body)
            ];
    
            $client = new Client();
            $response = $client->post($url, $options);
            $returnResponse->status = $response->getStatusCode();
            $returnResponse->body = json_decode(Psr7\stream_for($response->getBody()));
            return $returnResponse;
        
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $returnResponse->status = $response->getStatusCode();
            $returnResponse->body = json_decode(Psr7\stream_for($response->getBody()));
            return $returnResponse;
        }
        $returnResponse->body = 'Oops server error';
        return $returnResponse;
    }
}
