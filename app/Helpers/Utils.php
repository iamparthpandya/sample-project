<?php
use Illuminate\Support\Str;
use Illuminate\Http\Request;

if (! function_exists('deSlugify')) {
    function deSlugify($slug){
        return ucwords(str_replace('-', ' ', $slug));
    }
}

if (! function_exists('slugify')) {
    function slugify($title, $separator = '-')
    {
        return Str::slug($title, $separator);
    }
}

if (! function_exists('activateManu')) {
    function activateManu($menuName)
    {
        return request()->segment(1) === $menuName ? 'active' : '';
    }
}

if (! function_exists('authUser')) {
    function authUser($guard = 'web'){
        return auth()->guard($guard)->user();
    }
}

if (! function_exists('upload')) {
    function upload($file, $path = 'resources', $disk = 'local'){
        $filename = uniqid() . uniqid() .".". $file->extension();
        return $file->storeAs('uploads/'. $path, $filename, $disk);
    }
}

if (! function_exists('arrToStr')) {
    function arrToStr($arr, $separator = ','){
        return implode($separator, $arr);
    }
}

if (! function_exists('strToArr')) {
    function strToArr($str, $separator = ','){
        return explode($separator, $str);
    }
}

if (! function_exists('isAuthorized')) {
    function isAuthorizedUser($verifyId){
        $user = authUser();
        return $user->id === $verifyId;
    }
}

if (! function_exists('strAfterLast')) {
    function strAfterLast($str, $char){
        return Str::afterLast($str, $char);
    }
}

if (! function_exists('strLeft')) {
    function strLeft($str, $count){
        return substr ($str, 0, $count);
    }
}

if (! function_exists('strRight')) {
    function strRight($str, $count){
        return substr ($str, -$count);
    }
}


