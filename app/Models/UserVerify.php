<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVerify extends Model {

    protected $table = 'user_verify';

    protected $fillable = ['user_id','email_otp','mobile_otp','status','expiry_time','no_attempt','resend_otp_attempt','created_at', 'updated_at'];

    protected $hidden = ['created_at', 'updated_at'];

    
}
