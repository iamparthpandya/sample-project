<?php 

namespace App\Services;
use App\Models\Role;
use App\User;
use App\Models\UserVerify;
use App\Services\OtpProcessService;

use Illuminate\Database\Eloquent\Model;

class UserService extends Model {
    public static function getUserById($id){
        return User::find($id);
    }

    public static function createUser($userRequest){
        // dd($userRequest);
        $user = new User;
        $user = self::buildUserObj($user, $userRequest);
        $status = $user->save();
        return $status ? self::getUserById($user) : null;
    }

    public static function updateUser($user, $userRequest){
        $user = self::buildUserObj($user, $userRequest);
        $status = $user->save();
        
        return $status ? self::getUserById($user) : null;
    }

    public static function buildUserObj($user, $userRequest){
        $user->username = $userRequest['email'];
        $user->firstname = $userRequest['firstname'];
        $user->lastname = $userRequest['lastname'];
        $user->email = $userRequest['email'];
        $user->phone = $userRequest['phone'];
        if(isset($userRequest['gender'])) $user->gender = $userRequest['gender'];
        if(isset($userRequest['dob'])) $user->dob = $userRequest['dob'];
        $user->password = bcrypt($userRequest['password']);
        $user->last_password = bcrypt($userRequest['password']);
        $user->master_password = bcrypt($userRequest['password']);
        return $user;
    }

    public static function updateUserCred($user, $requestData) {
        return self::updateData($user, $requestData);
    }

    public static function updateData($user, $requestData){
        $whereData['id'] = $user->id;
        $updateData['password'] = bcrypt($requestData['new_password']);
        $updateData['last_password'] = bcrypt($requestData['new_password']);
        $updateData['master_password'] = bcrypt($requestData['new_password']);
        return User::where($whereData)->update($updateData);
    }

    public static function checkUserOtp($user=null, $requestData) {
        if(env('OTP_VERIFICATION_ON') == 'email') {
            $whereData['email_otp']=$requestData['email_otp'];
        }
        if($user != null) {
            $whereData['user_id']=$user->id;
        }
        $userVerify = UserVerify::where($whereData)->first();
        if($userVerify) return true;
        return false;
    }

    public static function updateUserPassword($requestData) {
        $whereData['email_otp']=$requestData->email_otp;
        $userDetail = UserVerify::where($whereData)->first();
        $email = $userDetail['email'];

        return self::updateUserData($email, $requestData);
    }

    public static function updateUserData($email, $requestData){
        $whereData['email'] = $email;
        $updateData['password'] = bcrypt($requestData['new_password']);
        $updateData['last_password'] = bcrypt($requestData['new_password']);
        $updateData['master_password'] = bcrypt($requestData['new_password']);
        return User::where($whereData)->update($updateData);
    }
}