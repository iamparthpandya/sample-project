<?php

namespace App\Services;
use DB;

class DbUtilityService 
{
    public static $INTERVAL_MICROSECOND = 'MICROSECOND';
    public static $INTERVAL_SECOND = 'SECOND';
    public static $INTERVAL_MINUTE = 'MINUTE';
    public static $INTERVAL_HOUR = 'HOUR';
    public static $INTERVAL_DAY = 'DAY';
    public static $INTERVAL_WEEK = 'WEEK';
    public static $INTERVAL_MONTH = 'MONTH';
    public static $INTERVAL_QUARTER = 'QUARTER';
    public static $INTERVAL_YEAR = 'YEAR';
   
    public static $HTTP_CREATED = 201;
    public static function getCurrentTime(){
        $query =  DB::table('dual')->selectRaw('now() as currentTime');
        $result = $query->get();
        if(count($result) > 0){
            return $result[0]->currentTime;
        }else{
            return date('Y-m-d H:i:s');
        }
    }
    
    public static function getCurrentDate(){
        $query =  DB::table('dual')->selectRaw('CURRENT_DATE() as currentDate');
        $result = $query->get();
        if(count($result) > 0){
            return $result[0]->currentDate;
        }else{
            return date('Y-m-d');
        }
	}
	
	public static function getCurrentTimePlus($seconds,$type){
        // DB::enableQueryLog();
        $query =  DB::table('dual')->selectRaw('DATE_ADD(now(), INTERVAL '.$seconds.' '.$type.') as currentTime');
        $result = $query->get();
        // dd(DB::getQueryLog());
        if(count($result) > 0){
            return $result[0]->currentTime;
        }else{
            return date('Y-m-d H:i:s');
        }
    }

    public static function getCurrentYear(){
        $query =  DB::table('dual')->selectRaw('year(now()) as currentYear');
        $result = $query->get();
        if(count($result) > 0){
            return $result[0]->currentYear;
        }else{
            return date('Y');
        }
    }
	
}
