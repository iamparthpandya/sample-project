<?php 

namespace App\Services;

use App\Services\UserVerifyService;
use App\Services\UtilityService;
use App\Util\SmsSender;
use Carbon\Carbon;
use DB;
use Strings;

use Event;
use App\Events\MobileSms;

class OtpProcessService{

    private static $otpMessageText = 'is SECRET OTP for changing the password. OTP is valid for 5 mins, please do not share this OTP with anyone.';
    private static $otpValidityTime = 900;

    public function __construct(){
    }

    public static function processOtp($user){
        if(env('OTP_VERIFICATION_ENABLE') == false) return 1;
        if(env('OTP_VERIFICATION_ON') == 'email') {
            $emailOtp = self::generateEmailOtp();
        }

        if(!isset($emailOtp)) return -1;
        if(isset($emailOtp)) {
            $status = UserVerifyService::insertUpdateData($user->id,$emailOtp,self::$otpValidityTime);
        }

        if($status){
            if(isset($emailOtp)) {
                $status = UtilityService::sendVerificationEmail($user->email, ['email_otp' => $emailOtp]);
            }
            return 0;
        }
        return -1;
    }

    public static function generateEmailOtp($digit = 6){
        if($digit > 9)$digit = 6;
        $randomOtp = mt_rand(pow(10,$digit - 1),pow(10,$digit) - 1);
        return $randomOtp;
    }

    public static function getUserByEmail($email){
        return User::where("email", $email)->first();
    }

    public static function sendUserOtp($email) {
        $user = self::getUserByEmail($email);
        if(!$user) return 0;

        if(env('OTP_VERIFICATION_ON') == 'email') {
            $emailOtp = self::generateEmailOtp();
        }

        if(isset($emailOtp)) {
            $status = UserVerifyService::insertUpdateOtpData($email,$emailOtp,self::$otpValidityTime);
        }
        if($status) {
            $status = UtilityService::sendVerificationEmail($email, ['email_otp' => $emailOtp]);
            return 1;
        }
        return -1;
    }

}

