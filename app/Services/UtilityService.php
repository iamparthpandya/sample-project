<?php

namespace App\Services;
use Illuminate\Support\Facades\Mail;
use App\Mail\ConfirmationMail;
use App\Mail\VerificationMail;

class UtilityService {
    public static function sendVerificationEmail($email, $emailOtp) {
        // dd($emailOtp);
        try {
            Mail::to($email)->send(new VerificationMail($emailOtp));
            return true;
        }
        catch(\Throwable $e) {
            return false;
        }
    }

    public static function sendRegistrationEmail($email) {
        try {
            Mail::to($email)->send(new RegistrationMail());
            return true;
        }
        catch(\Throwable $e) {
            return false;
        }
    }

    public static function sendLoginEmail($email) {
        try {
            Mail::to($email)->send(new LoginMail());
            return true;
        }
        catch(\Throwable $e) {
            return false;
        }
    }
}
