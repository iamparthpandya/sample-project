<?php

namespace App\Services;
use App\User;
use App\Models\UserVerify;
use App\Services\DbUtilityService;

class UserVerifyService {
	public function __construct(){
	}

	public static function findByUserId($userId){
		$whereData['user_id']=$userId;
        return UserVerify::where($whereData)->first();
	}

	public static function findByUserEmail($email){
		$whereData['email']=$email;
        return UserVerify::where($whereData)->first();
	}

	public static function insertUpdateData($userId,$emailOtp,$interval){
		$otpData = self::findByUserId($userId);
		if(is_null($otpData)){
			$insertData['user_id'] = $userId;
			$insertData['email_otp'] = $emailOtp;
			$expiry_time = DbUtilityService::getCurrentTimePlus($interval,DbUtilityService::$INTERVAL_SECOND);
			$insertData['expiry_time'] = $expiry_time;
			return self::insertData($insertData);
		}else{
			$whereData['user_id'] = $userId;
			$updateData['email_otp'] = $emailOtp;
			$expiry_time = DbUtilityService::getCurrentTimePlus($interval,DbUtilityService::$INTERVAL_SECOND);
			$updateData['expiry_time'] = $expiry_time;
			$updateData['no_attempt'] = $otpData['no_attempt'] + 1;
			$updateData['resend_otp_attempt'] = $otpData['resend_otp_attempt'] + 1;
			return self::updateData($whereData,$updateData);
		}
	}

	public static function insertUpdateOtpData($email,$emailOtp,$interval){
		$otpData = self::findByUserEmail($email);
		if(is_null($otpData)){
			$insertData['email'] = $email;
			$insertData['email_otp'] = $emailOtp;
			$expiry_time = DbUtilityService::getCurrentTimePlus($interval,DbUtilityService::$INTERVAL_SECOND);
			$insertData['expiry_time'] = $expiry_time;
			return self::insertData($insertData);
		}else{
			$whereData['email'] = $email;
			$updateData['email_otp'] = $emailOtp;
			$expiry_time = DbUtilityService::getCurrentTimePlus($interval,DbUtilityService::$INTERVAL_SECOND);
			$updateData['expiry_time'] = $expiry_time;
			$updateData['no_attempt'] = $otpData['no_attempt'] + 1;
			$updateData['resend_otp_attempt'] = $otpData['resend_otp_attempt'] + 1;
			return self::updateData($whereData,$updateData);
		}
	}

	public static function insertData($insertData){
		return UserVerify::create($insertData);
	}

	public static function updateData($whereData,$updateData){
		return UserVerify::where($whereData)->update($updateData);
	}

}