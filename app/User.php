<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname','phone','email','gender','password','last_password','master_password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','last_password','master_password','email_verified_at','created_at','updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }

    public static function buildCredential($request) {
        $credential['password'] = $request->password;

        if(is_numeric($request->identity)){
            $credential['phone'] = $request->identity;
        }else{
            $credential['email'] = $request->identity;
        }

        return $credential;
    }

    public static function loginRules() {
        return [
            'phone' => ['string', 'exists:users,phone'],
            'email' => ['email', 'string', 'exists:users,email'],
            'password' => ['required']
        ];
    }

    public static function getByIdentity($identity) {
        $where = is_numeric($identity) ? ['phone' => $identity] : ['email' => $identity];
        return static::where($where)->first();
    }

    public function verifyPassword($password) {
        return Hash::check($password, $this->password); 
    }

    public static function validateRules(){
        return [
            'firstname' => ['required', 'min:2', 'max:255'],
            'lastname' => ['required', 'min:2', 'max:255'],
            'email' => ['required', 'unique:users,email', 'max:255'],
            'phone' => ['required', 'unique:users,phone', 'digits:10'],
            'password' => ['required','min:6','max:20'],
            'gender' => ['in:Male,Female']
        ];
    }
}
