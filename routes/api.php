<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/user'], function() {
    // Token::not required
    Route::middleware(['headers'])->group(function () {
        Route::post('login', 'UserController@login')->name('user.login');  //for login
        Route::post('register', 'UserController@register')->name('user.register'); //for register

        Route::post('user/sendotp', 'UserController@forgetPassword'); //for sending otp on forgot password
        Route::post('password/update', 'UserController@updatePassword'); //for updating password
    });

    // Token::required
    Route::middleware(['headers','auth.jwt'])->group(function () {
        Route::post('logout', 'UserController@logout')->name('user.logout');
        Route::get('profile', 'UserController@profile')->name('user.profile');

        Route::post('{user}/sendotp','UserController@sendOtp')->name('user.details'); //for sending otp on change the password (post login)
        Route::put('update/cred','UserController@changePassword')->name('user.update'); //for changing the password (post login)
    });
});